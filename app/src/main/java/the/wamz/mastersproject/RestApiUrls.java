package the.wamz.mastersproject;

/**
 * Created by The on 13-Aug-15.
 */
public class RestApiUrls {
    // use as base url when debugging app using android emulator
    public static String baseUrl = "http://10.0.2.2:8080/projects/RestAPI-PHP/";

    // Authentication URLs
    public static String validatePatientIdentity = baseUrl+"validate";
    public static String createPassword = baseUrl+"register";
    public static String login = baseUrl+"login";
    public static String dashboard = baseUrl+"dashboard";

    // Profile URLs
    public static String editContactDetails = baseUrl+"profile/update";
    public static String updatePassword = baseUrl+"profile/security";

    // Appointment URLs
    public static String bookAppointment = baseUrl+"appointment/create";
    public static String updateAppointment = baseUrl+"appointment/update/";
    public static String cancelAppointment = baseUrl+"appointment/";
    public static String getAllTimeSlots = baseUrl+"timeslots";

    // Prescription URLs
    public static String reorderPrescription = baseUrl+"prescription/reorder/";
}
