package the.wamz.mastersproject.pickers;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

import the.wamz.mastersproject.activities.EditAppointmentActivity;

/**
 * Created by The on 05-Jul-15.
 */
public class EditAppointmentDatePickerFragment extends DialogFragment
{
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        // returning new instance of DatePickerDialog
        return new DatePickerDialog(getActivity(), (EditAppointmentActivity)getActivity(), year, month, day);
    }
}
