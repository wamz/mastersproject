package the.wamz.mastersproject.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by The on 30-Jul-15.
 */
public class Appointments implements Serializable
{
    private ArrayList<Appointment> listOfAppointments;

    public Appointments() {
    }

    public ArrayList<Appointment> getListOfAppointments() {
        return listOfAppointments;
    }

    public void setListOfAppointments(ArrayList<Appointment> listOfAppointments) {
        this.listOfAppointments = listOfAppointments;
    }
}
