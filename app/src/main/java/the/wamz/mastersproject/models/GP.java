package the.wamz.mastersproject.models;

import java.io.Serializable;

/**
 * Created by The on 30-Jul-15.
 */
public class GP implements Serializable
{
    private int id;
    private String name;

    public GP() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
