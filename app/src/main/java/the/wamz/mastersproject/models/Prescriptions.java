package the.wamz.mastersproject.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by The on 05-Sep-15.
 */
public class Prescriptions implements Serializable
{
    private ArrayList<Prescription> listOfPrescriptions;

    public Prescriptions() {
    }

    public ArrayList<Prescription> getListOfPrescriptions() {
        return listOfPrescriptions;
    }

    public void setListOfPrescriptions(ArrayList<Prescription> listOfPrescriptions) {
        this.listOfPrescriptions = listOfPrescriptions;
    }
}
