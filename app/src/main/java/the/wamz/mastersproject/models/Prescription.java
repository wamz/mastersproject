package the.wamz.mastersproject.models;

import java.io.Serializable;

/**
 * Created by The on 05-Sep-15.
 */
public class Prescription implements Serializable
{
    private int id;
    private String nameOfMedication;
    private String quantity;
    private String instructions;
    private int orderNumber;
    private int reorderTimes;
    private int gpId;
    private GPs listOfGPs;

    public Prescription() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameOfMedication() {
        return nameOfMedication;
    }

    public void setNameOfMedication(String nameOfMedication) {
        this.nameOfMedication = nameOfMedication;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public int getReorderTimes() {
        return reorderTimes;
    }

    public void setReorderTimes(int reorderTimes) {
        this.reorderTimes = reorderTimes;
    }

    public int getGpId() {
        return gpId;
    }

    public void setGpId(int gpId) {
        this.gpId = gpId;
    }

    public GPs getListOfGPs() {
        return listOfGPs;
    }

    public void setListOfGPs(GPs listOfGPs) {
        this.listOfGPs = listOfGPs;
    }
}
