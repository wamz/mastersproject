package the.wamz.mastersproject.models;

import java.io.Serializable;

/**
 * Created by The on 30-Jul-15.
 */
public class Appointment implements Serializable
{
    private int id;
    private String appointmentDate;
    private String appointmentTime;
    private int gpId;
    private int patientId;
    private GPs listOfGPs;
    private Patient patient;

    public Appointment() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public int getGpId() {
        return gpId;
    }

    public void setGpId(int gpId) {
        this.gpId = gpId;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public GPs getListOfGPs() {
        return listOfGPs;
    }

    public void setListOfGPs(GPs listOfGPs) {
        this.listOfGPs = listOfGPs;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
