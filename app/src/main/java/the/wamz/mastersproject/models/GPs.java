package the.wamz.mastersproject.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by The on 30-Jul-15.
 */
public class GPs implements Serializable
{
    private ArrayList<GP> listOfGPs;

    public GPs() {
    }

    public ArrayList<GP> getListOfGPs() {
        return listOfGPs;
    }

    public void setListOfGPs(ArrayList<GP> listOfGPs) {
        this.listOfGPs = listOfGPs;
    }
}
