package the.wamz.mastersproject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import the.wamz.mastersproject.models.Appointment;
import the.wamz.mastersproject.models.Appointments;
import the.wamz.mastersproject.models.GP;
import the.wamz.mastersproject.models.GPs;
import the.wamz.mastersproject.models.Patient;
import the.wamz.mastersproject.models.Prescription;
import the.wamz.mastersproject.models.Prescriptions;

/**
 * Created by The on 29-Aug-15.
 */
public class JSONParser {

    public Patient parsePatientDetails(JSONObject jsonObject, int patientId){
        Patient patient = null;
        try{
            // Checking if patient id is valid
            if(patientId == Integer.parseInt(jsonObject.getString("patientId"))) {
                // Parse patient details from json object to Patient model
                patient = new Patient();
                patient.setId(Integer.parseInt(jsonObject.getString("patientId")));
                patient.setAddressLine1(jsonObject.getString("addressLine1"));
                patient.setAddressLine2(jsonObject.getString("addressLine2"));
                patient.setTownCity(jsonObject.getString("townCity"));
                patient.setCounty(jsonObject.getString("county"));
                patient.setPostcode(jsonObject.getString("postcode"));
                patient.setMobileNumber(jsonObject.getString("mobileNumber"));
                patient.setTelephoneNumber(jsonObject.getString("telephoneNumber"));
                patient.setEmail(jsonObject.getString("email"));
                patient.setGpId(Integer.parseInt(jsonObject.getString("gpId")));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return patient;
    }

    public ArrayList<GP> parseGP(JSONArray jsonArray){
        ArrayList<GP> listOfGPs = null;
        try {
            listOfGPs = new ArrayList<GP>();
            // Iterating through JSONArray
            for (int i = 0; i < jsonArray.length(); i++) {
                // Parse GP details to GP model
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                GP gp = new GP();
                gp.setId(Integer.parseInt(jsonObject.getString("gpId")));
                gp.setName(jsonObject.getString("gpName"));
                // Add gp object to list of GPs
                listOfGPs.add(gp);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return listOfGPs;
    }

    public GPs parseListOfGPs(ArrayList<GP> listOfGPs){
        GPs gps = null;
        try {
            // Parse list of GPs to GPs model
            gps = new GPs();
            gps.setListOfGPs(listOfGPs);
        }catch (Exception e){
            e.printStackTrace();
        }
        return gps;
    }

    public ArrayList<Appointment> parseAppointment(JSONArray jsonArray, GPs gps, int patientId){
        ArrayList<Appointment> listOfAppointments = null;
        try{
            listOfAppointments = new ArrayList<Appointment>();
            // Iterating through JSONArray
            for (int i = 0; i < jsonArray.length(); i++){
                // iterating and getting the JSONObjects in the JSONArray
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                // Checking if patient id is valid
                if(patientId == Integer.parseInt(jsonObject.getString("patientId"))) {
                    // Parse appointment details from json object to Appointment model
                    Appointment appointment = new Appointment();
                    appointment.setId(Integer.parseInt(jsonObject.getString("id")));
                    appointment.setAppointmentDate(jsonObject.getString("appointmentDate"));
                    appointment.setAppointmentTime(jsonObject.getString("appointmentTime"));
                    appointment.setGpId(Integer.parseInt(jsonObject.getString("gpId")));
                    appointment.setPatientId(Integer.parseInt(jsonObject.getString("patientId")));
                    appointment.setListOfGPs(gps);
                    // Add appointment object to list of Appointments
                    listOfAppointments.add(appointment);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return listOfAppointments;
    }

    public Appointments parseListOfAppointments(ArrayList<Appointment> listOfAppointments){
         Appointments appointments = null;
        try{
            // Parse list of Appointments to Appointments model
            appointments = new Appointments();
            appointments.setListOfAppointments(listOfAppointments);
        }catch (Exception e){
            e.printStackTrace();
        }
        return appointments;
    }

    public ArrayList<Prescription> parsePrescription(JSONArray jsonArray, GPs gps, int patientId){
        ArrayList<Prescription> listOfPrescriptions = null;
        try{
            listOfPrescriptions = new ArrayList<Prescription>();
            // Iterating through JSONArray
            for (int i = 0; i < jsonArray.length(); i++) {
                // iterating and getting the JSONObjects in the JSONArray
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                // Checking if patient id is valid
                if (patientId == Integer.parseInt(jsonObject.getString("patientId"))) {
                    // Parse prescription details from json object to Prescription model
                    Prescription prescription = new Prescription();
                    prescription.setId(Integer.parseInt(jsonObject.getString("id")));
                    prescription.setNameOfMedication(jsonObject.getString("nameOfMedication"));
                    prescription.setQuantity(jsonObject.getString("quantity"));
                    prescription.setInstructions(jsonObject.getString("instructions"));
                    prescription.setOrderNumber(Integer.parseInt(jsonObject.getString("orderNumber")));
                    prescription.setReorderTimes(Integer.parseInt(jsonObject.getString("reorderTimes")));
                    prescription.setGpId(Integer.parseInt(jsonObject.getString("gpId")));
                    prescription.setListOfGPs(gps);
                    // Add prescription object to list of Prescriptions
                    listOfPrescriptions.add(prescription);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return listOfPrescriptions;
    }

    public Prescriptions parseListOfPrescriptions(ArrayList<Prescription> listOfPrescriptions){
        Prescriptions prescriptions = null;
        try{
            // Parse list of Prescriptions to Prescriptions model
            prescriptions = new Prescriptions();
            prescriptions.setListOfPrescriptions(listOfPrescriptions);
        }catch (Exception e){
            e.printStackTrace();
        }
        return prescriptions;
    }
}
