package the.wamz.mastersproject;

import java.util.ArrayList;

/**
 * Created by The on 09-Sep-15.
 */
public class TimeSlots {

    public static ArrayList<String> availableTimeSlots(){
        ArrayList<String> timeSlots = new ArrayList<String>();

        timeSlots.addAll(eightOclock());
        timeSlots.addAll(nineOclock());
        timeSlots.addAll(tenOclock());
        timeSlots.addAll(elevenOclock());
        timeSlots.addAll(twelveOclock());
        timeSlots.addAll(oneOclock());
        timeSlots.addAll(twoOclock());
        timeSlots.addAll(threeOclock());
        timeSlots.addAll(fourOclock());
        timeSlots.addAll(fiveOclock());

        return timeSlots;
    }

    private static ArrayList<String> eightOclock(){
        ArrayList<String> timeSlots = new ArrayList<String>();

        timeSlots.add("08:00");
        timeSlots.add("08:10");
        timeSlots.add("08:20");
        timeSlots.add("08:30");
        timeSlots.add("08:40");
        timeSlots.add("08:50");

        return timeSlots;
    }

    private static ArrayList<String> nineOclock(){
        ArrayList<String> timeSlots = new ArrayList<String>();

        timeSlots.add("09:00");
        timeSlots.add("09:10");
        timeSlots.add("09:20");
        timeSlots.add("09:30");
        timeSlots.add("09:40");
        timeSlots.add("09:50");

        return timeSlots;
    }

    private static ArrayList<String> tenOclock(){
        ArrayList<String> timeSlots = new ArrayList<String>();

        timeSlots.add("10:00");
        timeSlots.add("10:10");
        timeSlots.add("10:20");
        timeSlots.add("10:30");
        timeSlots.add("10:40");
        timeSlots.add("10:50");

        return timeSlots;
    }

    private static ArrayList<String> elevenOclock(){
        ArrayList<String> timeSlots = new ArrayList<String>();

        timeSlots.add("11:00");
        timeSlots.add("11:10");
        timeSlots.add("11:20");
        timeSlots.add("11:30");
        timeSlots.add("11:40");
        timeSlots.add("11:50");

        return timeSlots;
    }

    private static ArrayList<String> twelveOclock(){
        ArrayList<String> timeSlots = new ArrayList<String>();

        timeSlots.add("12:00");
        timeSlots.add("12:10");
        timeSlots.add("12:20");
        timeSlots.add("12:30");
        timeSlots.add("12:40");
        timeSlots.add("12:50");

        return timeSlots;
    }

    private static ArrayList<String> oneOclock(){
        ArrayList<String> timeSlots = new ArrayList<String>();

        timeSlots.add("13:00");
        timeSlots.add("13:10");
        timeSlots.add("13:20");
        timeSlots.add("13:30");
        timeSlots.add("13:40");
        timeSlots.add("13:50");

        return timeSlots;
    }

    private static ArrayList<String> twoOclock(){
        ArrayList<String> timeSlots = new ArrayList<String>();

        timeSlots.add("14:00");
        timeSlots.add("14:10");
        timeSlots.add("14:20");
        timeSlots.add("14:30");
        timeSlots.add("14:40");
        timeSlots.add("14:50");

        return timeSlots;
    }

    private static ArrayList<String> threeOclock(){
        ArrayList<String> timeSlots = new ArrayList<String>();

        timeSlots.add("15:00");
        timeSlots.add("15:10");
        timeSlots.add("15:20");
        timeSlots.add("15:30");
        timeSlots.add("15:40");
        timeSlots.add("15:50");

        return timeSlots;
    }

    private static ArrayList<String> fourOclock(){
        ArrayList<String> timeSlots = new ArrayList<String>();

        timeSlots.add("16:00");
        timeSlots.add("16:10");
        timeSlots.add("16:20");
        timeSlots.add("16:30");
        timeSlots.add("16:40");
        timeSlots.add("16:50");

        return timeSlots;
    }

    private static ArrayList<String> fiveOclock(){
        ArrayList<String> timeSlots = new ArrayList<String>();

        timeSlots.add("17:00");
        timeSlots.add("17:10");
        timeSlots.add("17:20");
        timeSlots.add("17:30");
        timeSlots.add("17:40");
        timeSlots.add("17:50");

        return timeSlots;
    }
}
