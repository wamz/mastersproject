package the.wamz.mastersproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import the.wamz.mastersproject.R;

/**
 * Created by The on 09-Sep-15.
 */
public class TimeSlotsAdapter extends ArrayAdapter<String>
{
    ArrayList<String> timeSlots;
    int listitem_layout;
    Context context;
    LayoutInflater layoutInflater;

    public TimeSlotsAdapter(Context context, int listitem_layout, ArrayList<String> timeSlots) {
        super(context, listitem_layout, timeSlots);

        this.context = context;
        this.listitem_layout = listitem_layout;
        this.timeSlots = timeSlots;

        // inflating the layout of the activity_listview_listitem
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // Method used to optimize listview
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Using viewholder to recycle inflate layout views
        ViewHolder viewHolder;

        // Checking if convertView is null
        if (convertView == null) {
            // inflating the activity_listview_listitem layout and assigning it to a convertView
            convertView = layoutInflater.inflate(listitem_layout, null);
            viewHolder = new ViewHolder();

            // Assigning the viewholder to the list item textview
            viewHolder.timeSlotsTextView = (TextView)
                    convertView.findViewById(R.id.activity_book_appointment_listview_listitem_textview);

            // setting a tag for the convertView so it can recycled for reuse in the listview
            convertView.setTag(viewHolder);
        }
        else {
            // recycling the viewholder
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // setting the data to be displayed in each row of the listview
        viewHolder.timeSlotsTextView.setText(timeSlots.get(position).toString());

        return convertView;
    }

    // this class holds references to the relevant views in the dashboard layout
    static class ViewHolder{
        public TextView timeSlotsTextView;
    }
}
