package the.wamz.mastersproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import the.wamz.mastersproject.R;
import the.wamz.mastersproject.models.Appointment;

/**
 * Created by The on 24-Aug-15.
 */
public class AppointmentsAdapter extends ArrayAdapter<Appointment>
{
    ArrayList<Appointment> listOfAppointments;
    int listitem_layout;
    Context context;
    LayoutInflater layoutInflater;

    public AppointmentsAdapter(Context context, int listitem_layout, ArrayList<Appointment> listOfAppointments) {
        super(context, listitem_layout, listOfAppointments);

        this.context = context;
        this.listitem_layout = listitem_layout;
        this.listOfAppointments = listOfAppointments;

        // inflating the layout of the activity_listview_listitem
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // Method used to optimize listview
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Using viewholder to recycle inflate layout views
        ViewHolder viewHolder;

        // Checking if convertView is null
        if (convertView == null) {
            // inflating the activity_listview_listitem layout and assigning it to a convertView
            convertView = layoutInflater.inflate(listitem_layout, null);
            viewHolder = new ViewHolder();

            // Assigning the viewholder to the list item textview
            viewHolder.appointmentTextView = (TextView)
                    convertView.findViewById(R.id.activity_listview_listitem_textview);

            // setting a tag for the convertView so it can recycled for reuse in the listview
            convertView.setTag(viewHolder);
        }
        else {
            // recycling the viewholder
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // setting the data to be displayed in each row of the listview
        viewHolder.appointmentTextView.setText(listOfAppointments.get(position).getAppointmentDate() + " @ " +
        listOfAppointments.get(position).getAppointmentTime());

        return convertView;
    }

    // this class holds references to the relevant views in the dashboard layout
    static class ViewHolder{
        public TextView appointmentTextView;
    }
}
