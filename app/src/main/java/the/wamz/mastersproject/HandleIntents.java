package the.wamz.mastersproject;

import android.content.Intent;

import the.wamz.mastersproject.models.Appointment;
import the.wamz.mastersproject.models.GPs;
import the.wamz.mastersproject.models.Patient;
import the.wamz.mastersproject.models.Prescription;
import the.wamz.mastersproject.models.Prescriptions;

/**
 * Created by The on 12-Sep-15.
 */
public class HandleIntents
{
    // This method gets the passed Patient object from the intent
    public static Patient getPatientDetailsFromIntent(Intent getIntent){
        Patient patient = null;
        try {
            // getting data passed via the intent
            Intent intent = getIntent;
            // Checking if intent object has passed data
            if (intent != null) {
                // Extracting patient object from intent and assigning it to patient object
                patient = (Patient) intent.getSerializableExtra("Patient");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        // Returning patient object
        return patient;
    }

    // This method gets the passed Prescriptions object from the intent
    public static Prescriptions getPrescriptionsFromIntent(Intent getIntent){
        Prescriptions prescriptions = null;
        try {
            // getting data passed via the intent
            Intent intent = getIntent;
            // Checking if intent object has passed data
            if (intent != null) {
                // Extracting prescriptions object from intent and assigning it to prescriptions object
                prescriptions = (Prescriptions) intent.getSerializableExtra("Prescriptions");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        // Returning prescriptions object
        return prescriptions;
    }

    // This method gets the passed GPs object from the intent
    public static GPs getGPsFromIntent(Intent getIntent){
        GPs gps = null;
        try {
            // getting data passed via the intent
            Intent intent = getIntent;
            // Checking if intent object has passed data
            if (intent != null) {
                // Extracting GPs object from intent and assigning it to GPs object
                gps = (GPs) intent.getSerializableExtra("GPs");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        // Returning GPs object
        return gps;
    }

    // This method gets the passed appointment object from the intent
    public static Appointment getAppointmentDetailsFromIntent(Intent getIntent){
        Appointment appointment = null;
        try {
            // getting data passed via the intent
            Intent intent = getIntent;
            // Checking if intent object has passed data
            if (intent != null) {
                // Extracting appointment object from intent and assigning it to appointment object
                appointment = (Appointment) intent.getSerializableExtra("appointment");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        // Returning appointment object
        return appointment;
    }

    // This method gets the passed Prescription object from the intent
    public static Prescription getPrescriptionDetailsFromIntent(Intent getIntent){
        Prescription prescription = null;
        try {
            // getting data passed via the intent
            Intent intent = getIntent;
            // Checking if intent object has passed data
            if (intent != null) {
                // Extracting prescription object from intent and assigning it to prescription object
                prescription = (Prescription) intent.getSerializableExtra("Prescription");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        // Returning prescription object
        return prescription;
    }
}
