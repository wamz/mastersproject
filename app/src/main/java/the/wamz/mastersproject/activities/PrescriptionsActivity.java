package the.wamz.mastersproject.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import the.wamz.mastersproject.HandleIntents;
import the.wamz.mastersproject.R;
import the.wamz.mastersproject.SharedPreferencesFileName;
import the.wamz.mastersproject.adapters.PrescriptionsAdapter;
import the.wamz.mastersproject.models.Patient;
import the.wamz.mastersproject.models.Prescription;
import the.wamz.mastersproject.models.Prescriptions;

/**
 * Created by The on 05-Sep-15.
 */
public class PrescriptionsActivity extends AppCompatActivity
{
    private SharedPreferences sharedPreferences;
    private ListView prescriptionsListView;
    private PrescriptionsAdapter prescriptionsAdapter;
    private Prescriptions prescriptions;
    private Patient patient;
    private Intent intent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescriptions);

        sharedPreferences = getSharedPreferences(SharedPreferencesFileName.credsFile, MODE_PRIVATE);
//        Toast.makeText(this,  String.valueOf(sharedPreferences.getInt("PatientId", 0)), Toast.LENGTH_LONG).show();
//        Toast.makeText(this, sharedPreferences.getString("NhsNumber", null), Toast.LENGTH_LONG).show();

        // Getting data passed via intent
        intent = getIntent();
        patient = HandleIntents.getPatientDetailsFromIntent(intent);
        prescriptionsListView = (ListView) findViewById(R.id.activity_prescriptions_listview);
        displayingPrescriptionsListView();
        prescriptionsListItemClickListener();
    }

    private void displayingPrescriptionsListView(){
        prescriptions = HandleIntents.getPrescriptionsFromIntent(intent);
        // Instantiating a new PrescriptionsAdapter object and displaying list of prescriptions
        prescriptionsAdapter = new PrescriptionsAdapter(this, R.layout.activity_listview_listitem,
                prescriptions.getListOfPrescriptions());
        // setting adapter for prescriptionsListView
        prescriptionsListView.setAdapter(prescriptionsAdapter);
    }

    private void prescriptionsListItemClickListener(){
        prescriptionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Capturing selected prescription from list of prescriptions
                Prescription prescription = (Prescription) prescriptionsAdapter.getItem(position);
                // Navigate to PrescriptionDetailsActivity passing selected prescription object
                Intent intent = new Intent(getApplicationContext(), PrescriptionDetailsActivity.class);
                intent.putExtra("Prescription", prescription);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_prescriptions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        // handling clicks on action bar items
        switch (menuItem.getItemId()){
            case R.id.menu_prescriptions_action_settings:
                // Navigating to SettingsActivity passing patient and prescriptions objects
                Intent settingsActivityIntent = new Intent(this, SettingsActivity.class);
                settingsActivityIntent.putExtra("Patient", patient);
                settingsActivityIntent.putExtra("Prescriptions", prescriptions);
                startActivity(settingsActivityIntent);
                return true;
            case R.id.menu_prescriptions_action_dashboard:
                // Navigate to dashboard activity
                Intent dashboardActivityIntent = new Intent(this, DashBoardActivity.class);
                startActivity(dashboardActivityIntent);
                finish();
                return true;
            case R.id.menu_prescriptions_action_logout:
                // log out of application, delete all data stored in shared preferences file
                // and navigate to login activity
                sharedPreferences.edit().clear().commit();
                Intent loginActivityIntent = new Intent(this, LoginActivity.class);
                startActivity(loginActivityIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }
}
