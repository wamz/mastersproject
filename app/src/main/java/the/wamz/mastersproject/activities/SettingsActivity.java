package the.wamz.mastersproject.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import the.wamz.mastersproject.HandleIntents;
import the.wamz.mastersproject.R;
import the.wamz.mastersproject.SharedPreferencesFileName;
import the.wamz.mastersproject.models.Patient;
import the.wamz.mastersproject.models.Prescriptions;

/**
 * Created by The on 26-Aug-15.
 */
public class SettingsActivity extends AppCompatActivity
{
    private Patient patient;
    private Prescriptions prescriptions;
    private SharedPreferences sharedPreferences;
    private Intent intent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        sharedPreferences = getSharedPreferences(SharedPreferencesFileName.credsFile, MODE_PRIVATE);
//        Toast.makeText(this,  String.valueOf(sharedPreferences.getInt("PatientId", 0)), Toast.LENGTH_LONG).show();
//        Toast.makeText(this, sharedPreferences.getString("NhsNumber", null), Toast.LENGTH_LONG).show();

        intent = getIntent();
        patient = HandleIntents.getPatientDetailsFromIntent(intent);
        prescriptions = HandleIntents.getPrescriptionsFromIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        // handling clicks on action bar items
        switch (menuItem.getItemId()){
            case R.id.menu_settings_action_dashboard:
                // Navigate to dashboard activity
                Intent dashboardActivityIntent = new Intent(this, DashBoardActivity.class);
                startActivity(dashboardActivityIntent);
                finish();
                return true;
            case R.id.menu_settings_action_logout:
                // log out of application and navigate to login activity
                sharedPreferences.edit().clear().commit();
                Intent loginActivityIntent = new Intent(this, LoginActivity.class);
                startActivity(loginActivityIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void updatePassword(View view){
        Intent intent = new Intent(this, UpdatePasswordActivity.class);
        startActivity(intent);
    }

    public void viewContactDetails(View view){
        Intent intent = new Intent(this, ViewContactDetailsActivity.class);
        intent.putExtra("Patient", patient);
        intent.putExtra("Prescriptions", prescriptions);
        startActivity(intent);
    }
}
