package the.wamz.mastersproject.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import the.wamz.mastersproject.HttpConnection;
import the.wamz.mastersproject.JSONParser;
import the.wamz.mastersproject.R;
import the.wamz.mastersproject.RestApiUrls;
import the.wamz.mastersproject.SharedPreferencesFileName;
import the.wamz.mastersproject.adapters.AppointmentsAdapter;
import the.wamz.mastersproject.models.Appointment;
import the.wamz.mastersproject.models.Appointments;
import the.wamz.mastersproject.models.GP;
import the.wamz.mastersproject.models.GPs;
import the.wamz.mastersproject.models.Patient;
import the.wamz.mastersproject.models.Prescription;
import the.wamz.mastersproject.models.Prescriptions;

/**
 * Created by The on 24-Jul-15.
 */
public class DashBoardActivity extends AppCompatActivity
{
    private SharedPreferences sharedPreferences;
    private ListView appointmentsListView;
    private AppointmentsAdapter appointmentsAdapter;
    private Patient patient;
    private Prescriptions prescriptions;
    private GPs gps;
    private Appointments appointments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

//        sharedPreferences = getSharedPreferences(SharedPreferencesFileName.credsFile, MODE_PRIVATE);
//        Toast.makeText(this,  String.valueOf(sharedPreferences.getInt("PatientId", 0)), Toast.LENGTH_LONG).show();
//        Toast.makeText(this, sharedPreferences.getString("NhsNumber", null), Toast.LENGTH_LONG).show();

        // Executing DashBoardAsyncTask and returning all relevant data
        new DashBoardAsyncTask().execute(headerParams());

        appointmentsListView = (ListView) findViewById(R.id.activity_dashboard_listview);
        appointmentsListItemClickListener();
    }

    private ArrayList<String> headerParams() {
        ArrayList<String> params = new ArrayList<String>();
        try {
            sharedPreferences = getSharedPreferences(SharedPreferencesFileName.credsFile, MODE_PRIVATE);
            params.add(String.valueOf(sharedPreferences.getInt("PatientId", 0)));
        }catch (Exception e){
            e.printStackTrace();
        }
        return params;
    }

    private void appointmentsListItemClickListener(){
        // Setting a item click listener for items in the appointmentsListView
        appointmentsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Appointment appointment = (Appointment) appointmentsAdapter.getItem(position);

                        Intent intent = new Intent(getApplicationContext(), ViewAppointmentDetailsActivity.class);
                        intent.putExtra("appointment", appointment);
                        intent.putExtra("GPs", gps);
                        intent.putExtra("Patient", patient);
                        intent.putExtra("Prescriptions", prescriptions);
                        startActivity(intent);
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // handling clicks on action bar items
        switch (menuItem.getItemId()){
            case R.id.menu_dashboard_action_book_appointment:
                /* Navigating to BookAppointmentActivity passing GPs, Appointments, Prescriptions
                and Patient objects*/
                Intent bookAppointmentActivityIntent = new Intent(this, BookAppointmentActivity.class);
                bookAppointmentActivityIntent.putExtra("GPs", gps);
                bookAppointmentActivityIntent.putExtra("Appointments", appointments);
                bookAppointmentActivityIntent.putExtra("Patient", patient);
                bookAppointmentActivityIntent.putExtra("Prescriptions", prescriptions);
                startActivity(bookAppointmentActivityIntent);
                return true;
            case R.id.menu_dashboard_action_settings:
                /* Navigating to BookAppointmentActivity passing Prescriptions and Patient objects*/
                Intent settingsActivityIntent = new Intent(this, SettingsActivity.class);
                settingsActivityIntent.putExtra("Patient", patient);
                settingsActivityIntent.putExtra("Prescriptions", prescriptions);
                startActivity(settingsActivityIntent);
                return true;
            case  R.id.menu_dashboard_action_prescriptions:
                /* Navigating to BookAppointmentActivity passing Prescriptions and Patient objects*/
                Intent prescriptionsActivityIntent = new Intent(this, PrescriptionsActivity.class);
                prescriptionsActivityIntent.putExtra("Prescriptions", prescriptions);
                prescriptionsActivityIntent.putExtra("Patient", patient);
                startActivity(prescriptionsActivityIntent);
                return true;
            case R.id.menu_dashboard_action_logout:
                /*Deleting all data stored in the shared preferences and navigating to the login activity*/
                sharedPreferences.edit().clear().commit();
                Intent loginActivityIntent = new Intent(this, LoginActivity.class);
                startActivity(loginActivityIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    // AsyncTask class for getting all appointments from the server via REST API
    private class DashBoardAsyncTask extends AsyncTask<ArrayList<String>, Void, String>
    {
        @Override
        protected String doInBackground(ArrayList<String>... params) {
            String response = null;
            try {
                // Executing async task in background thread and returning response
                response = getAllRelevantData(params);
            }catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            try {
                if (response != null){
                    // Getting json data from response
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject patientDetailsJsonObject = jsonObject.getJSONObject("patientDetails");
                    JSONArray appointmentsJsonArray = jsonObject.getJSONArray("appointments");
                    JSONArray gpDetailsJsonArray = jsonObject.getJSONArray("gpDetails");
                    JSONArray prescriptionsJsonArray = jsonObject.getJSONArray("prescriptions");
                    // Object for the SharedPreferences class
                    sharedPreferences = getSharedPreferences(SharedPreferencesFileName.credsFile, MODE_PRIVATE);
                    int patientId = sharedPreferences.getInt("PatientId", 0);
                    // object for the JSONParser class
                    JSONParser jsonParser = new JSONParser();
                    // Parsing Patient data from patientDetails jsonObject in json response
                    patient = jsonParser.parsePatientDetails(patientDetailsJsonObject, patientId);
                    // Parsing GP data from gpDetails array in json response
                    ArrayList<GP> listOfGPs = jsonParser.parseGP(gpDetailsJsonArray);
                    gps = jsonParser.parseListOfGPs(listOfGPs);
                    // Parsing appointment data from appointments array in json response
                    ArrayList<Appointment> listOfAppointments = jsonParser.parseAppointment(appointmentsJsonArray,
                            gps, patientId);
                    appointments = jsonParser.parseListOfAppointments(listOfAppointments);
                    // Parsing prescription data from prescriptions array in json response
                    ArrayList<Prescription> listOfPrescriptions = jsonParser.parsePrescription(prescriptionsJsonArray, gps,
                            patientId);
                    prescriptions = jsonParser.parseListOfPrescriptions(listOfPrescriptions);
                    // Instantiating a new AppointmentsAdapter object
                    appointmentsAdapter = new AppointmentsAdapter(getApplicationContext(),
                            R.layout.activity_listview_listitem, appointments.getListOfAppointments());
                    // setting adapter for appointmentsListView
                    appointmentsListView.setAdapter(appointmentsAdapter);
                }
                else {
                    Toast.makeText(getApplicationContext(), "No Current Appointments", Toast.LENGTH_LONG).show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        private String getAllRelevantData(ArrayList<String>... params){
            String response = null;
            try {
                // Handling HTTP request to server
                URL url = new URL(RestApiUrls.dashboard);
                HttpURLConnection httpURLConnection = HttpConnection.connectToServer(url, "GET");
                // header data
                String authorization = params[0].get(0);
                // Checking if authorization variable is empty or not
                if (authorization != null){
                    // Assigning header data to Authorization header
                    httpURLConnection.setRequestProperty("Authorization", authorization);
                }
                // Check if HTTP response status code is 200
                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    // Get response from server
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    // Processing response
                    response = HttpConnection.getResponse(inputStream);
                }
                httpURLConnection.disconnect();
            }catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }
    }
}
