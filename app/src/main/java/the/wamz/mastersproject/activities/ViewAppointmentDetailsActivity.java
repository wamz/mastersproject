package the.wamz.mastersproject.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import the.wamz.mastersproject.HandleIntents;
import the.wamz.mastersproject.HttpConnection;
import the.wamz.mastersproject.R;
import the.wamz.mastersproject.RestApiUrls;
import the.wamz.mastersproject.SharedPreferencesFileName;
import the.wamz.mastersproject.models.Appointment;
import the.wamz.mastersproject.models.GP;
import the.wamz.mastersproject.models.GPs;
import the.wamz.mastersproject.models.Patient;
import the.wamz.mastersproject.models.Prescriptions;

/**
 * Created by The on 26-Aug-15.
 */
public class ViewAppointmentDetailsActivity extends AppCompatActivity
{
    private TextView appointmentDateTextView;
    private TextView appointmentTimeTextView;
    private TextView gpNameTextView;
    private Appointment appointment;
    private GPs gps;
    private SharedPreferences sharedPreferences;
    private Patient patient;
    private Prescriptions prescriptions;
    private Intent intent;

    // Activity constructor
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_appointment_details);

        uiElementsSetUp();
        intent = getIntent();
        appointment = HandleIntents.getAppointmentDetailsFromIntent(intent);
        displayAppointmentDetails(appointment);
        gps = HandleIntents.getGPsFromIntent(intent);
        patient = HandleIntents.getPatientDetailsFromIntent(intent);
        prescriptions = HandleIntents.getPrescriptionsFromIntent(intent);
    }

    // This method sets up activity user interface
    private void uiElementsSetUp(){
        appointmentDateTextView = (TextView) findViewById(R.id.activity_view_appointment_details_appointment_date_textview);
        appointmentTimeTextView = (TextView) findViewById(R.id.activity_view_appointment_details_appointment_time_textview);
        gpNameTextView = (TextView) findViewById(R.id.activity_view_appointment_details_appointment_gpname_textview);
    }

    // This method displays appointment data in the appropriate textviews
    private void displayAppointmentDetails(Appointment appointment){
        try {
            // displaying the appointment date and time
            appointmentDateTextView.setText(appointment.getAppointmentDate());
            appointmentTimeTextView.setText(appointment.getAppointmentTime());

            // Iterating through the list of GPs
            for (GP gp : appointment.getListOfGPs().getListOfGPs()) {
                // checking for appointments with a matching gpId to the ones stored in the list of GPs
                if (gp.getId() == appointment.getGpId()) {
                    // displaying the GP's name
                    gpNameTextView.setText(gp.getName().toString());
                }
            }
            // handling exceptions
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /* This method stores the Request Header params for the
    * cancel appointment async tasks in an Arraylist */
    private ArrayList<String> cancelAppointmentHeaderParams() {
        ArrayList<String> params = new ArrayList<String>();
        try {
            sharedPreferences = getSharedPreferences(SharedPreferencesFileName.credsFile, MODE_PRIVATE);
            params.add(String.valueOf(sharedPreferences.getInt("PatientId", 0)));
        }catch (Exception e){
            e.printStackTrace();
        }
        return params;
    }

    // This method handles the displaying of the action bar items on the action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // inflating the action bar items so they are visible on the action bar
        getMenuInflater().inflate(R.menu.menu_view_details, menu);
        return true;
    }

    // This method handles the action bar item clicks
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.menu_view_details_edit_details_action_bar_item:
                /* When edit appointment action bar item is clicked,
                    * Navigate to the edit appointment activity, passing an appointment object*/
                try {
                    Intent editAppointmentActivityIntent = new Intent(this, EditAppointmentActivity.class);
                    editAppointmentActivityIntent.putExtra("appointment", appointment);
                    editAppointmentActivityIntent.putExtra("GPs", gps);
                    editAppointmentActivityIntent.putExtra("Prescriptions", prescriptions);
                    editAppointmentActivityIntent.putExtra("Patient", patient);
                    startActivity(editAppointmentActivityIntent);
                }catch (Exception e){
                    e.printStackTrace();
                }
                return true;
            case R.id.menu_view_details_delete_action_bar_item:
                /* When cancel appointment action bar item is clicked,
                    * Execute the cancel appointment async task, passing the Request Header params*/
                try {
                    new CancelAppointmentAsyncTask().execute(cancelAppointmentHeaderParams());
                }catch (Exception e){
                    e.printStackTrace();
                }
                return true;
            case R.id.menu_view_details_settings_action_bar_item:
                // Navigate to the settings activity
                try {
                    Intent settingsActivityIntent = new Intent(this, SettingsActivity.class);
                    settingsActivityIntent.putExtra("Patient", patient);
                    startActivity(settingsActivityIntent);
                    finish();
                }catch (Exception e){
                    e.printStackTrace();
                }
                return true;
            case R.id.menu_view_details_prescriptions_action_bar_item:
                Intent prescriptionsActivityIntent = new Intent(this, PrescriptionsActivity.class);
                prescriptionsActivityIntent.putExtra("Prescriptions", prescriptions);
                startActivity(prescriptionsActivityIntent);
                finish();
                return true;
            case R.id.menu_view_details_logout_action_bar_item:
                sharedPreferences.edit().clear().commit();
                Intent loginActivityIntent = new Intent(this, LoginActivity.class);
                startActivity(loginActivityIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    // AsyncTask class for handling the cancelling of an appointment via with the REST API
    private class CancelAppointmentAsyncTask extends AsyncTask<ArrayList<String>, Void, String>
    {
        // This method runs in a background thread
        @Override
        protected String doInBackground(ArrayList<String>... params) {
            String response = null;
            try {
                // Executing async task in background thread and returning response
                response = cancelAppointment(params);
            }catch (Exception e){
                e.printStackTrace();
            }
            // Returning the response data from the server
            return response;
        }

        // This method outputs response data from the server
        @Override
        protected void onPostExecute(String response) {
            try {
                // Checking if response from REST API is not empty
                if (response != null) {
                    // Getting the json data from response
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    // Displaying success message
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    // Navigating to dashboard activity
                    Intent dashboardActivityIntent = new Intent(getApplicationContext(), DashBoardActivity.class);
                    startActivity(dashboardActivityIntent);
                    finish();
                }
                else{
                    // Error message to be displayed if response from REST API is empty
                    Toast.makeText(getApplicationContext(), "Error: Appointment Not Cancelled! \n Please Try Again",
                            Toast.LENGTH_LONG).show();
                }
                // Handling exceptions
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        // Method that handles the cancelling of an appointment
        private String cancelAppointment(ArrayList<String>... params){
            String response = null;
            try {
                // Handling HTTP request to server
                URL url = new URL(RestApiUrls.cancelAppointment+appointment.getId());
                HttpURLConnection httpURLConnection = HttpConnection.connectToServer(url, "DELETE");
                // Request Header params
                String authorization = params[0].get(0);
                // Checking Request Header params is not empty
                if (authorization != null){
                    // Assigning the Request Header params to the Authorization Header
                    httpURLConnection.setRequestProperty("Authorization", authorization);
                }
                // Checking for a 200 response code from the server
                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    // Getting the response from the server
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    // Processing response
                    response = HttpConnection.getResponse(inputStream);
                }
                httpURLConnection.disconnect();
                // Handling exceptions
            }catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }
    }
}
