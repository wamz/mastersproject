package the.wamz.mastersproject.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import the.wamz.mastersproject.HandleIntents;
import the.wamz.mastersproject.HttpConnection;
import the.wamz.mastersproject.R;
import the.wamz.mastersproject.RestApiUrls;
import the.wamz.mastersproject.models.Patient;
import the.wamz.mastersproject.models.Prescriptions;

/**
 * Created by The on 25-Jul-15.
 */
public class EditContactDetailsActivity extends AppCompatActivity
{
    private EditText addressLine1EditText;
    private EditText addressLine2EditText;
    private EditText townCityEditText;
    private EditText countyEditText;
    private EditText postcodeEditText;
    private EditText mobileNumberEditText;
    private EditText telephoneNumberEditText;
    private EditText emailAddressEditText;
    private Patient patient;
    private Prescriptions prescriptions;
    private SharedPreferences sharedPreferences;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact_details);

        uiElementsSetUp();
        intent = getIntent();
        patient = HandleIntents.getPatientDetailsFromIntent(intent);
        prescriptions = HandleIntents.getPrescriptionsFromIntent(intent);
        displayContactDetails(patient);
    }

    private void uiElementsSetUp(){
        addressLine1EditText = (EditText) findViewById(R.id.activity_edit_contact_details_address_line1_edittext);
        addressLine2EditText = (EditText) findViewById(R.id.activity_edit_contact_details_address_line2_edittext);
        townCityEditText = (EditText) findViewById(R.id.activity_edit_contact_details_town_city_edittext);
        countyEditText = (EditText) findViewById(R.id.activity_edit_contact_details_county_edittext);
        postcodeEditText = (EditText) findViewById(R.id.activity_edit_contact_details_postcode_edittext);
        mobileNumberEditText = (EditText) findViewById(R.id.activity_edit_contact_details_mobile_number_edittext);
        telephoneNumberEditText = (EditText) findViewById(R.id.activity_edit_contact_details_telephone_number_edittext);
        emailAddressEditText = (EditText) findViewById(R.id.activity_edit_contact_details_email_edittext);
    }

    // This method displays patient data in the appropriate textviews
    private void displayContactDetails(Patient patient){
        try {
            // Displaying current contact details
            addressLine1EditText.setText(patient.getAddressLine1().toString());
            addressLine2EditText.setText(patient.getAddressLine2().toString());
            townCityEditText.setText(patient.getTownCity().toString());
            countyEditText.setText(patient.getCounty().toString());
            postcodeEditText.setText(patient.getPostcode().toString());
            mobileNumberEditText.setText(patient.getMobileNumber().toString());
            telephoneNumberEditText.setText(patient.getTelephoneNumber().toString());
            emailAddressEditText.setText(patient.getEmail().toString());
            // handling exceptions
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    // Method to store data to be posted in an arraylist
    private ArrayList<String> postParams() {
        ArrayList<String> params = new ArrayList<String>();
        try {
            params.add(String.valueOf(patient.getId()));
            params.add(addressLine1EditText.getText().toString());
            params.add(addressLine2EditText.getText().toString());
            params.add(townCityEditText.getText().toString());
            params.add(countyEditText.getText().toString());
            params.add(postcodeEditText.getText().toString());
            params.add(mobileNumberEditText.getText().toString());
            params.add(telephoneNumberEditText.getText().toString());
            params.add(emailAddressEditText.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // handling clicks on action bar items
        switch (menuItem.getItemId()){
            case R.id.menu_edit_settings_action_bar_item:
                // Navigating to the SettingsActivity passing a patient object
                Intent settingsActivityIntent = new Intent(this, SettingsActivity.class);
                settingsActivityIntent.putExtra("Patient", patient);
                settingsActivityIntent.putExtra("Prescriptions", prescriptions);
                startActivity(settingsActivityIntent);
                finish();
                return true;
            case R.id.menu_edit_dashboard_action_bar_item:
                Intent dashboardActivityIntent = new Intent(this, DashBoardActivity.class);
                startActivity(dashboardActivityIntent);
                finish();
                return true;
            case R.id.menu_edit_prescriptions_action_bar_item:
                Intent prescriptionsActivityIntent = new Intent(this, PrescriptionsActivity.class);
                prescriptionsActivityIntent.putExtra("Prescriptions", prescriptions);
                startActivity(prescriptionsActivityIntent);
                finish();
                return true;
            case R.id.menu_edit_logout_action_bar_item:
                sharedPreferences.edit().clear().commit();
                Intent loginActivityIntent = new Intent(this, LoginActivity.class);
                startActivity(loginActivityIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void updateContactDetailsButtonClicked(View view){
        // Execute EditContactAsyncTask and edit contact details
        new EditContactDetailsAsyncTask().execute(postParams());
    }

    private class EditContactDetailsAsyncTask extends AsyncTask<ArrayList<String>, Void, String>
    {
        @Override
        protected String doInBackground(ArrayList<String>... params) {
            String response = null;
            try {
                // Executing async task in background thread and returning response
                response = editContactDetails(params);
            }catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            try {
                // Checking if response from server is empty or not
                if (response != null){
                    // Parse json data in response
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    // Display success message
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    // Navigate to the DashBoard Activity
                    Intent intent = new Intent(getApplicationContext(), DashBoardActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    String message = "Error: Contact Details Not Updated \n Please Try Again!";
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        private String editContactDetails(ArrayList<String>... params){
            String response = null;
            try {
                // Handling HTTP requests to server
                URL url = new URL(RestApiUrls.editContactDetails);
                HttpURLConnection httpURLConnection = HttpConnection.connectToServer(url, "POST");
                // Header data
                String authorization = params[0].get(0);
                // Checking if authorization variable is empty or not
                if (authorization != null){
                    // Assign header data to Authorization header
                    httpURLConnection.setRequestProperty("Authorization", authorization);
                }
                // data to be sent to the server
                String postParams = "addressLine1=" + URLEncoder.encode(params[0].get(1), "UTF-8") +
                        "&addressLine2=" + URLEncoder.encode(params[0].get(2), "UTF-8") +
                        "&townCity=" + URLEncoder.encode(params[0].get(3), "UTF-8") +
                        "&county=" + URLEncoder.encode(params[0].get(4), "UTF-8") +
                        "&postcode=" + URLEncoder.encode(params[0].get(5), "UTF-8") +
                        "&mobileNumber=" + URLEncoder.encode(params[0].get(6), "UTF-8") +
                        "&telephoneNumber=" + URLEncoder.encode(params[0].get(7), "UTF-8") +
                        "&email=" + URLEncoder.encode(params[0].get(8), "UTF-8");

                httpURLConnection.setFixedLengthStreamingMode(postParams.getBytes().length);
                // Sending request and data to server
                HttpConnection.sendToServer(httpURLConnection, postParams);
                // Checking if HTTP response status code is 200
                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    // Getting response from server
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    // Processing response
                    response = HttpConnection.getResponse(inputStream);
                }
                httpURLConnection.disconnect();
            }catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }
    }
}
