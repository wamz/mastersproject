package the.wamz.mastersproject.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import the.wamz.mastersproject.HandleIntents;
import the.wamz.mastersproject.HttpConnection;
import the.wamz.mastersproject.R;
import the.wamz.mastersproject.RestApiUrls;
import the.wamz.mastersproject.models.GP;
import the.wamz.mastersproject.models.Prescription;

/**
 * Created by The on 05-Sep-15.
 */
public class PrescriptionDetailsActivity extends AppCompatActivity
{
    private SharedPreferences sharedPreferences;
    private TextView nameOfMedicationTextView;
    private TextView quantityTextView;
    private TextView instructionsTextView;
    private TextView reorderTimesTextView;
    private TextView gpNameTextView;
    private Button reorderPrescriptionButton;
    private Prescription prescription;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription_details);

        uiElementsSetUp();
        intent = getIntent();
        prescription = HandleIntents.getPrescriptionDetailsFromIntent(intent);
        displayPrescriptionDetails(prescription);
        isReorderOptionAvailable();
    }

    private void uiElementsSetUp(){
        nameOfMedicationTextView = (TextView) findViewById(R.id.activity_prescription_details_name_of_medication_textview);
        quantityTextView = (TextView) findViewById(R.id.activity_prescription_details_quantity_textview);
        instructionsTextView = (TextView) findViewById(R.id.activity_prescription_details_instructions_textview);
        reorderTimesTextView = (TextView) findViewById(R.id.activity_prescription_details_reorder_times_textview);
        gpNameTextView = (TextView) findViewById(R.id.activity_prescription_details_gp_name_textview);
        reorderPrescriptionButton = (Button) findViewById(R.id.activity_prescription_reorder_prescription_button);
    }

    private void displayPrescriptionDetails(Prescription prescription){
        // Displaying prescription details
        nameOfMedicationTextView.setText(prescription.getNameOfMedication().toString());
        quantityTextView.setText(prescription.getQuantity().toString());
        instructionsTextView.setText(prescription.getInstructions().toString());
        reorderTimesTextView.setText(String.valueOf(prescription.getReorderTimes()));

        // Iterating through the list of GPs
        for (GP gp : prescription.getListOfGPs().getListOfGPs()) {
            // checking for prescription with a matching gpId to the ones stored in the list of GPs
            if (gp.getId() == prescription.getGpId()) {
                // displaying the GP's name
                gpNameTextView.setText(gp.getName().toString());
            }
        }
    }

    // MEthod to check if reorder times is above zero
    private void isReorderOptionAvailable(){
        if (prescription.getReorderTimes() == 0){
            reorderPrescriptionButton.setEnabled(false);
        }
        else {
            reorderPrescriptionButton.setEnabled(true);
        }
    }

    // Method to store data to be posted in an arraylist
    private ArrayList<String> postParams() {
        ArrayList<String> details = new ArrayList<String>();
        try {
            int reorder = prescription.getReorderTimes();
            reorder--;

            details.add(prescription.getNameOfMedication().toString());
            details.add(prescription.getQuantity().toString());
            details.add(prescription.getInstructions().toString());
            details.add(String.valueOf(prescription.getOrderNumber()));
            details.add(String.valueOf(reorder));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return details;
    }

    public void reorderPrescriptionButtonClicked(View view){
        try {
            new PrescriptionReOrderAsyncTask().execute(postParams());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // Class for handling Asynchronous Tasks
    private class PrescriptionReOrderAsyncTask extends AsyncTask<ArrayList<String>, Void, String>
    {
        // Executing the task to prescriptionReOrder
        @Override
        protected String doInBackground(ArrayList<String>... params) {
            String response = null;
            try{
                // Executing async task in background thread and returning response
                response = prescriptionReOrder(params);
            }catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }

        // Retrieving the response from the doInBackground method
        @Override
        protected void onPostExecute(String response) {
            try{
                // Checking to make sure the response outputted from the doInBackground is not empty
                if (response != null){
                    // Getting string response from REST API and storing it in a json object
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    // Display Success message
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    // Navigating to the Dashboard Activity
                    Intent intent = new Intent(getApplicationContext(), DashBoardActivity.class);
                    startActivity(intent);
                }
                else {
                    // Error message to be displayed if response returned by doInBackground method is empty
                    String message = "Error: Prescription Not Re-ordered!";
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        private String prescriptionReOrder(ArrayList<String>... params){
            String response = null;
            try{
                // Handling HTTP requests to server
                // Passing the id of the selected prescription as a parameter of the POST request url
                URL url = new URL(RestApiUrls.reorderPrescription+prescription.getId());
                HttpURLConnection httpURLConnection = HttpConnection.connectToServer(url, "POST");
                // data to be sent to the server
                String postParams = "nameOfMedication=" + URLEncoder.encode(params[0].get(0), "UTF-8") +
                        "&quantity=" + URLEncoder.encode(params[0].get(1), "UTF-8") +
                        "&instructions=" + URLEncoder.encode(params[0].get(2), "UTF-8") +
                        "&orderNumber=" + URLEncoder.encode(params[0].get(3), "UTF-8") +
                        "&reorderTimes=" + URLEncoder.encode(params[0].get(4), "UTF-8");

                httpURLConnection.setFixedLengthStreamingMode(postParams.getBytes().length);
                // Sending data to server
                HttpConnection.sendToServer(httpURLConnection, postParams);
                // Checking if response code from REST-API is 200
                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    // Get response stream from server
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    // Processing response
                    response = HttpConnection.getResponse(inputStream);
                }
                httpURLConnection.disconnect();
            }catch(Exception e){
                e.printStackTrace();
            }
            return response;
        }
    }
}
