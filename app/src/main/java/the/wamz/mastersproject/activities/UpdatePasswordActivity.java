package the.wamz.mastersproject.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import the.wamz.mastersproject.HttpConnection;
import the.wamz.mastersproject.R;
import the.wamz.mastersproject.RestApiUrls;
import the.wamz.mastersproject.SharedPreferencesFileName;

/**
 * Created by The on 26-Aug-15.
 */
public class UpdatePasswordActivity extends AppCompatActivity
{
    private EditText currentPasswordEditText;
    private EditText newPasswordEditText;
    private EditText confirmNewPasswordEditText;
    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);

        uiElementsSetUp();
    }

    private void uiElementsSetUp(){
        currentPasswordEditText = (EditText) findViewById(R.id.activity_update_password_current_password_edittext);
        newPasswordEditText = (EditText) findViewById(R.id.activity_update_password_new_password_edittext);
        confirmNewPasswordEditText = (EditText) findViewById(R.id.activity_update_password_confirm_new_password_edittext);
    }

    // Checking if login data fields are empty
    private boolean isPasswordDataEntered(){
        if (currentPasswordEditText.getText().toString().matches("") ||
                newPasswordEditText.getText().toString().matches("") ||
                confirmNewPasswordEditText.getText().toString().matches("")){
            return false;
        }
        else {
            return true;
        }
    }

    // Method to check if passwords entered match
    private boolean doPasswordsMatch(){
        if (newPasswordEditText.getText().toString().equals(confirmNewPasswordEditText.getText().toString())){
            return true;
        }
        else{
            return false;
        }
    }

    // Method to store data to be posted in an arraylist
    private ArrayList<String> updatePasswordPostParams() {
        ArrayList<String> details = new ArrayList<String>();
        try {
            // Adding post params to arraylist
            sharedPreferences = getSharedPreferences(SharedPreferencesFileName.credsFile, MODE_PRIVATE);
            details.add(sharedPreferences.getString("NhsNumber", null));
            details.add(currentPasswordEditText.getText().toString());
            details.add(newPasswordEditText.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return details;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void updatePassword(View view){
        if (isPasswordDataEntered()){
            // Checking if passwords entered match
            if (doPasswordsMatch()) {
                // execute UpdatePasswordAsyncTask to update password
                new UpdatePasswordAsyncTask().execute(updatePasswordPostParams());
            }
            else{
                // Error message if passwords entered do not match
                Toast.makeText(this, "Passwords Do Not Match!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class UpdatePasswordAsyncTask extends AsyncTask<ArrayList<String>, Void, String>
    {
        @Override
        protected String doInBackground(ArrayList<String>... params) {
            String response = null;
            try {
                // Executing async task in background thread and returning response
                response = updatePassword(params);
            }catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            try {
                // Checking if response from server is empty or not
                if (response != null){
                    // Parse json data in response
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    // Display Success message
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    // Navigating to the Dashboard Activity
                    Intent intent = new Intent(getApplicationContext(), DashBoardActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    // Display Error message
                    String message = "Error: Password Not Updated!";
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        // Method to handle the posting of password details to the REST-API
        private String updatePassword(ArrayList<String>... params){
            String response = null;
            try {
                // Handling HTTP Requests to server
                URL url = new URL(RestApiUrls.updatePassword);
                HttpURLConnection httpURLConnection = HttpConnection.connectToServer(url, "POST");
                // Header data
                String authorization = params[0].get(0);
                // Checking if authorization variable is empty or not
                if (authorization != null){
                    // Assigning header data to Authorization header
                    httpURLConnection.setRequestProperty("Authorization", authorization);
                }
                // data to be sent to the server
                String postParams = "currentPassword=" + URLEncoder.encode(params[0].get(1), "UTF-8") +
                        "&newPassword=" + URLEncoder.encode(params[0].get(2), "UTF-8");

                httpURLConnection.setFixedLengthStreamingMode(postParams.getBytes().length);
                // Sending data to server
                HttpConnection.sendToServer(httpURLConnection, postParams);
                // Checking if HTTP response status code is 200
                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    // Getting response from server
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    // Processing response
                    response = HttpConnection.getResponse(inputStream);
                }
                httpURLConnection.disconnect();
            }catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }
    }
}
