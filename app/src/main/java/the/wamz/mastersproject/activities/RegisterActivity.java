package the.wamz.mastersproject.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import the.wamz.mastersproject.HttpConnection;
import the.wamz.mastersproject.R;
import the.wamz.mastersproject.RestApiUrls;
import the.wamz.mastersproject.SharedPreferencesFileName;
import the.wamz.mastersproject.models.Patient;

/**
 * Created by The on 24-Jul-15.
 */
public class RegisterActivity extends AppCompatActivity
{

    private EditText nhsNumberEditText;
    private EditText dateOfBirthEditText;
    private EditText postcodeEditText;
    private EditText passwordEditText;
    private EditText confirmPasswordEditText;
    private Button registerButton;
    Patient patient;
    private boolean connection;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        uiElementsSetUp();
        patient = new Patient();
        sharedPreferences = getSharedPreferences(SharedPreferencesFileName.credsFile, MODE_PRIVATE);
    }

    private void uiElementsSetUp() {
        nhsNumberEditText = (EditText) findViewById(R.id.activity_register_nhs_number_editText);
        dateOfBirthEditText = (EditText) findViewById(R.id.activity_register_date_of_birth_editText);
        postcodeEditText = (EditText) findViewById(R.id.activity_register_postcode_editText);
        passwordEditText = (EditText) findViewById(R.id.activity_register_password_editText);
        confirmPasswordEditText = (EditText) findViewById(R.id.activity_register_confirm_password_editText);
        registerButton = (Button) findViewById(R.id.activity_register_register_button);
    }

    // Method to check if validate details have been entered
    private boolean isValidateDataEntered() {
        if (nhsNumberEditText.getText().toString().matches("") || dateOfBirthEditText.getText().toString().matches("") ||
                postcodeEditText.getText().toString().matches("")) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isPasswordDataEntered() {
        if (passwordEditText.getText().toString().matches("") || confirmPasswordEditText.getText().toString().matches("")) {
            return false;
        } else {
            return true;
        }
    }

    // Method to check if passwords entered match
    private boolean doPasswordsMatch(){
        if (passwordEditText.getText().toString().equals(confirmPasswordEditText.getText().toString())){
            return true;
        }
        else{
            return false;
        }
    }

    private ArrayList<String> validatePatientIdentityPostParams() {
        ArrayList<String> validateIdentityDetails = new ArrayList<String>();
        try {
            validateIdentityDetails.add(nhsNumberEditText.getText().toString());
            validateIdentityDetails.add(dateOfBirthEditText.getText().toString());
            validateIdentityDetails.add(postcodeEditText.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return validateIdentityDetails;
    }

    private ArrayList<String> registerPostParams() {
        ArrayList<String> registerDetails = new ArrayList<String>();
        try {
            registerDetails.add(nhsNumberEditText.getText().toString());
            registerDetails.add(passwordEditText.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return registerDetails;
    }

    public void validateDetailsButtonClicked(View view) {
        try {
            // Checking if valid was entered into validate patient identity fields
            if (isValidateDataEntered()) {
                // Executing ValidatePatientIdentityAsyncTask to validate patient's identity
                Toast.makeText(this, "Validating...", Toast.LENGTH_SHORT).show();
                new ValidatePatientIdentityAsyncTask().execute(validatePatientIdentityPostParams());
            } else {
                Toast.makeText(this, "All fields are required!", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void registerButtonClicked(View view) {
        // Checking if valid password data was entered into password fields
        if (isPasswordDataEntered()) {
            // Checking if passwords match
            if (doPasswordsMatch()) {
                // Executing CreatePasswordAsyncTask to register patient and store their password
                Toast.makeText(this, "Registration in Process...", Toast.LENGTH_SHORT).show();
                new CreatePasswordAsyncTask().execute(registerPostParams());
            }
            else {
                Toast.makeText(this, "Passwords do not match!", Toast.LENGTH_LONG).show();
            }
        }
        else {
            Toast.makeText(this, "All password fields are required!", Toast.LENGTH_LONG).show();
        }
    }

    private class ValidatePatientIdentityAsyncTask extends AsyncTask<ArrayList<String>, Void, String>
    {
        @Override
        protected String doInBackground(ArrayList<String>... params) {
            String response = null;
            try {
                // Executing async task in background thread and returning response
                response = validate(params);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            try {
                if (response != null) {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean error = jsonObject.getBoolean("error");

                    // Checking if password exists
                    if (error){
                        String message = jsonObject.getString("message");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        // Navigate to login activity
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intent);
                    }
                    else {
                        // Display user's name
                        String patientName = jsonObject.getString("patientName");
                        patient.setName(patientName);
                        Toast.makeText(getApplicationContext(), "Hello " + patient.getName(),
                                Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "Please create a password for your account!",
                                Toast.LENGTH_LONG).show();
                        // Enabling password fields
                        passwordEditText.setEnabled(true);
                        confirmPasswordEditText.setEnabled(true);
                        registerButton.setEnabled(true);
                    }
                }
                else{
                    // Error message to be displayed
                    String message = "Error: Invalid Details \n Please Try Again!";
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        private String validate(ArrayList<String>... params) {
            String response = null;
            try {
                URL url = new URL(RestApiUrls.validatePatientIdentity);
                HttpURLConnection httpURLConnection = HttpConnection.connectToServer(url, "POST");
                // data to be posted to server
                String postParams = "nhsNumber=" + URLEncoder.encode(params[0].get(0), "UTF-8") +
                        "&dateOfBirth=" + URLEncoder.encode(params[0].get(1), "UTF-8") +
                        "&postcode=" + URLEncoder.encode(params[0].get(2), "UTF-8");

                httpURLConnection.setFixedLengthStreamingMode(postParams.getBytes().length);
                // Sending data to server
                HttpConnection.sendToServer(httpURLConnection, postParams);
                // Checking if HTTP response status code is 200
                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    // Getting response from server
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    // Processing response
                    response = HttpConnection.getResponse(inputStream);
                }
                httpURLConnection.disconnect();
            }
            catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }
    }

    private class CreatePasswordAsyncTask extends AsyncTask<ArrayList<String>, Void, String>
    {
        @Override
        protected String doInBackground(ArrayList<String>... params) {
            String response = null;
            try{
                // Executing async task in background thread and returning response
                response = createPassword(params);
            }catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            try{
                if (response != null){
                    // Getting json data from response
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    String id = jsonObject.getString("patientId");

                    int patientId = Integer.parseInt(id);
                    patient.setId(patientId);
                    // Storing patient identifying data in shared preferences file
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("NhsNumber", nhsNumberEditText.getText().toString());
                    editor.putInt("PatientId", patient.getId());
                    editor.commit();
                    // Display success message
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    // Navigate to DashBoard Activity
                    Intent intent = new Intent(getApplicationContext(), DashBoardActivity.class);
                    startActivity(intent);
                }
                else{
                    String message = "Error: Unable to complete registration process \n Please Try Again!";
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        private String createPassword(ArrayList<String>... params){
            String response = null;
            try{
                // Handling the HTTP request to the server
                URL url = new URL(RestApiUrls.createPassword);
                HttpURLConnection httpURLConnection = HttpConnection.connectToServer(url, "POST");
                // data to be sent to server
                String postParams = "nhsNumber=" + URLEncoder.encode(params[0].get(0), "UTF-8") +
                        "&password=" + URLEncoder.encode(params[0].get(1), "UTF-8");

                httpURLConnection.setFixedLengthStreamingMode(postParams.getBytes().length);
                // Sending data to server
                HttpConnection.sendToServer(httpURLConnection, postParams);
                // Checking if HTTP response status code is 201
                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
                    // Get response from server
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    // Processing response
                    response = HttpConnection.getResponse(inputStream);
                }
                httpURLConnection.disconnect();
            }catch(Exception e){
                e.printStackTrace();
            }
            return response;
        }
    }
}