package the.wamz.mastersproject.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import the.wamz.mastersproject.HttpConnection;
import the.wamz.mastersproject.R;
import the.wamz.mastersproject.RestApiUrls;
import the.wamz.mastersproject.SharedPreferencesFileName;

public class LoginActivity extends AppCompatActivity
{
    private EditText nhsNumberEditText;
    private EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        uiElementsSetUp();

        userLoggedIn();
    }

    // Setting up User Interface elements
    private void uiElementsSetUp(){
        nhsNumberEditText = (EditText) findViewById(R.id.activity_login_nhs_number_editText);
        passwordEditText = (EditText) findViewById(R.id.activity_login_password_editText);
    }

    // Checking if login data fields are empty
    private boolean isLoginDataEntered(){
        if (nhsNumberEditText.getText().toString().matches("") || passwordEditText.getText().toString().matches("") ||
                nhsNumberEditText.getText().toString().equals("NHS Number (Format: 123-456-7890)") ||
                passwordEditText.getText().toString().equals("Password")){
            return false;
        }
        else {
            return true;
        }
    }

    // Method to store login details to be posted to server
    private ArrayList<String> loginPostParams() {
        ArrayList<String> loginDetails = new ArrayList<String>();
        try {
            // Storing login details in arraylist
            loginDetails.add(nhsNumberEditText.getText().toString());
            loginDetails.add(passwordEditText.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loginDetails;
    }

    // Method to check if user is already logged in
    private boolean isUserLoggedIn(){
        // Accessing shared preferences file
        SharedPreferences sharedPreferences = getSharedPreferences(SharedPreferencesFileName.credsFile, MODE_PRIVATE);
        // Retrieving data stored in shared preferences file
        int patientId = sharedPreferences.getInt("PatientId", 0);
        String nhsNumber = sharedPreferences.getString("NhsNumber", null);

        // Checking if data is empty or not
        if (patientId != 0 && nhsNumber != null){
            return true;
        }
        else{
            return false;
        }
    }

    private void userLoggedIn(){
        // Checking if user is already logged in
        if (isUserLoggedIn()){
            // Navigate to the Dashboard Activity
            Intent dashboardActivityIntent = new Intent(this, DashBoardActivity.class);
            startActivity(dashboardActivityIntent);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // handling clicks on action bar items
        switch (menuItem.getItemId()){
            case R.id.menu_login_action_register:
                Intent intent = new Intent(this, RegisterActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    // Click listener for the login button
    public void loginButtonClicked(View view){
        // Checking if login details are empty or not
        if (isLoginDataEntered()){
            // Execute LoginAsyncTask and attempt to login into application
            new LoginAsyncTask().execute(loginPostParams());
        }
        else {
            // Display message
            Toast.makeText(this, "All login details required!", Toast.LENGTH_LONG).show();
        }
    }

    // Class for handling Asynchronous Tasks
    private class LoginAsyncTask extends AsyncTask<ArrayList<String>, Void, String>
    {
        // Executing the task to login
        @Override
        protected String doInBackground(ArrayList<String>... params) {
            String response = null;
            try{
                // Executing async task in background thread and returning response
                response = login(params);
            }catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }

        // Retrieving the response from the doInBackground method
        @Override
        protected void onPostExecute(String response) {
            try{
                // Checking to make sure the response outputted from the doInBackground is not empty
                if (response != null){
                    // Storing the response in a JSONObject
                    // Extracting the json data from response using JSONObject
                    JSONObject jsonObject = new JSONObject(response);
                    int patientId = Integer.parseInt(jsonObject.getString("patientId"));
                    String patientName = jsonObject.getString("patientName");
                    // Displaying logged in user's name
                    Toast.makeText(getApplicationContext(), "Hello " + patientName, Toast.LENGTH_LONG).show();
                    // Storing the user's credentials
                    storeUserCredentials(patientId);
                    // Navigating to the Dashboard Activity
                    Intent intent = new Intent(getApplicationContext(), DashBoardActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    // Error message to be displayed if response returned by doInBackground method is empty
                    String message = "Error: Invalid Login Details \n Please Try Again!";
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        private String login(ArrayList<String>... params){
            String response = null;
            try{
                // Setting the http connection to the server URL
                URL url = new URL(RestApiUrls.login);
                HttpURLConnection httpURLConnection = HttpConnection.connectToServer(url, "POST");
                // data to be sent to the REST-API
                String postParams = "nhsNumber=" + URLEncoder.encode(params[0].get(0), "UTF-8") +
                        "&password=" + URLEncoder.encode(params[0].get(1), "UTF-8");

                httpURLConnection.setFixedLengthStreamingMode(postParams.getBytes().length);
                // Sending data to server
                HttpConnection.sendToServer(httpURLConnection, postParams);
                // Checking if response code from REST-API is 200
                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    // Get response from server
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    // Processing response
                    response = HttpConnection.getResponse(inputStream);
                }
                httpURLConnection.disconnect();
            }catch(Exception e){
                e.printStackTrace();
            }
            return response;
        }

        private void storeUserCredentials(int patientId){
            // Storing user identifying data in a SharedPreferences file
            SharedPreferences sharedPreferences = getSharedPreferences(
                    SharedPreferencesFileName.credsFile, MODE_PRIVATE);
            // Storing patient identifying data
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("NhsNumber", nhsNumberEditText.getText().toString());
            editor.putInt("PatientId", patientId);
            editor.commit();
        }
    }
}
