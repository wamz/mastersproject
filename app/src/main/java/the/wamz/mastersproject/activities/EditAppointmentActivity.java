package the.wamz.mastersproject.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import the.wamz.mastersproject.HandleIntents;
import the.wamz.mastersproject.HttpConnection;
import the.wamz.mastersproject.R;
import the.wamz.mastersproject.RestApiUrls;
import the.wamz.mastersproject.SharedPreferencesFileName;
import the.wamz.mastersproject.TimeSlots;
import the.wamz.mastersproject.adapters.TimeSlotsAdapter;
import the.wamz.mastersproject.models.Appointment;
import the.wamz.mastersproject.models.GP;
import the.wamz.mastersproject.models.GPs;
import the.wamz.mastersproject.models.Patient;
import the.wamz.mastersproject.models.Prescriptions;
import the.wamz.mastersproject.pickers.EditAppointmentDatePickerFragment;

/**
 * Created by The on 25-Jul-15.
 */
public class EditAppointmentActivity extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener
{
    private SharedPreferences sharedPreferences;
    private TextView selectDateTextView;
    private Spinner selectDoctorSpinner;
    private GPs gps;
    private int gpId;
    private String selectedTime;
    private ListView availableTimeSlotsListView;
    private TimeSlotsAdapter timeSlotsAdapter;
    private ArrayList<Appointment> listOfBookedTimeSlots;
    private Appointment appointment;
    private Patient patient;
    private Prescriptions prescriptions;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_appointment);

        Toast.makeText(this, "Please select a Date to view Available Time Slots", Toast.LENGTH_LONG).show();
        // Executing async task to get all booked timeslots from server
        new GetAllTimeSlotsAsyncTask().execute();
        uiElementsSetUp();
        // Retrieving data passed via intent
        intent = getIntent();
        patient = HandleIntents.getPatientDetailsFromIntent(intent);
        appointment = HandleIntents.getAppointmentDetailsFromIntent(intent);
        prescriptions = HandleIntents.getPrescriptionsFromIntent(intent);
        displayAppointmentDetails(appointment);
        displayListOfGPs();
        availableTimeSlotsListItemClickListener();
    }

    private void uiElementsSetUp(){
        // Setting User Interface elements
        selectDateTextView = (TextView) findViewById(R.id.activity_edit_appointment_select_date_textview);
        selectDoctorSpinner = (Spinner) findViewById(R.id.activity_edit_appointment_select_doctor_spinner);
        availableTimeSlotsListView = (ListView) findViewById(R.id.activity_edit_appointment_listview);
    }

    private void displayAppointmentDetails(Appointment appointment){
        // Displaying details of selected appointment
        try {
            selectDateTextView.setText(appointment.getAppointmentDate().toString());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        // Capturing the selected date
        monthOfYear = monthOfYear+1;
        populateTextViewWithSelectedDate(year, monthOfYear, dayOfMonth);
        // Displaying list of available time slots for selected date
            displayAvailableTimeSlots();
            timeSlotsAdapter = new TimeSlotsAdapter(this, R.layout.activity_book_appointment_listview_listitem,
                    displayAvailableTimeSlots());
            availableTimeSlotsListView.setAdapter(timeSlotsAdapter);
    }

    // selectDateTextView onclick method
    public void selectDate(View view){
        // Displaying the DatePicker
        DialogFragment datePickerFragment = new EditAppointmentDatePickerFragment();
        datePickerFragment.show(getSupportFragmentManager(), "DatePicker");
    }

    // Method to populate selectDateTextView with chosen date
    private void populateTextViewWithSelectedDate(int year, int month, int day){
        if (month < 10 && day < 10) {
            selectDateTextView.setText(year + "-" + "0" + month + "-" + "0" + day);
        }
        else if (month < 10) {
            selectDateTextView.setText(year + "-" + "0" + month + "-" + day);
        }
        else if (day < 10) {
            selectDateTextView.setText(year + "-" + month + "-" + "0" + day);
        }
        else {
            selectDateTextView.setText(year + "-" + month + "-" + day);
        }
    }

    // Method to return list of booked time slots
    private ArrayList<String> listOfBookedTimeSlots(){
        ArrayList<String> listOfBookedTimes = null;
        try{
            listOfBookedTimes = new ArrayList<String>();
            // Iterating through listOfBookedTimeSlots
            for (Appointment appointment : listOfBookedTimeSlots) {
                // Checking if selected date matches date retrieved from server
                if (selectDateTextView.getText().toString().equals(appointment.getAppointmentDate().toString())) {
                    // Storing all booked time slots for selected date in listOfBookedTimes
                    listOfBookedTimes.add(appointment.getAppointmentTime());
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return listOfBookedTimes;
    }

    // Method for displaying available time slots
    private ArrayList<String> displayAvailableTimeSlots(){
        ArrayList<String> availableTimeSlots = null;
        try {
            // Adding all time slots
            availableTimeSlots = new ArrayList<String>(TimeSlots.availableTimeSlots());
            availableTimeSlots.addAll(listOfBookedTimeSlots());
            // Removing all booked time slots from list of available time slots
            ArrayList<String> intersection = new ArrayList<String>(TimeSlots.availableTimeSlots());
            intersection.retainAll(listOfBookedTimeSlots());
            availableTimeSlots.removeAll(intersection);
        }catch (Exception e){
            e.printStackTrace();
        }
        return availableTimeSlots;
    }

    private void availableTimeSlotsListItemClickListener(){
        availableTimeSlotsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Capturing selected time slot
                selectedTime = timeSlotsAdapter.getItem(position);
                Toast.makeText(getApplicationContext(), selectedTime, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void displayListOfGPs(){
        try {
            // Retrieving GPs passed from intent
            gps = HandleIntents.getGPsFromIntent(intent);
            ArrayList<String> arrayList = new ArrayList<String>();
            // Adding GP names to arraylist
            for (GP gp : gps.getListOfGPs()) {
                arrayList.add(gp.getName().toString());
            }
            // Checking list of GPs is not empty
            if (gps.getListOfGPs() != null) {
                // initializing adapter for selectDoctorSpinner
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_dropdown_item, arrayList);
                // setting selectDoctorSpinner adapter
                selectDoctorSpinner.setAdapter(adapter);
                // setting onitemclicklistener for when items in selectDoctorSpinner are selected
                selectDoctorSpinner.setOnItemSelectedListener(this);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // Capturing selected GP
        TextView textView = (TextView) view;
        // Iterating through list of GPs
        for (GP gp : gps.getListOfGPs()) {
            // Checking if selected GP name matches GP name in list of GPs
            if (textView.getText().toString().equals(gp.getName().toString())){
                // Capturing id of selected GP
                gpId = gp.getId();
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    // Checking if date and time for appointment have been set
    private boolean isPostDataEntered(){
        if (selectDateTextView.getText().toString().equals("Select Date") || selectedTime == null){
            return false;
        }
        else {
            return true;
        }
    }

    // Method to store data to be posted to server in an arraylist
    private ArrayList<String> postParams() {
        ArrayList<String> details = new ArrayList<String>();
        try {
            sharedPreferences = getSharedPreferences(SharedPreferencesFileName.credsFile, MODE_PRIVATE);
            details.add(String.valueOf(sharedPreferences.getInt("PatientId", 0)));
            details.add(selectDateTextView.getText().toString());
            details.add(selectedTime);
            details.add(String.valueOf(gpId));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return details;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // handling clicks on action bar items
        switch (menuItem.getItemId()){
            case R.id.menu_edit_settings_action_bar_item:
                // Navigating to the SettingsActivity passing a patient and prescriptions objects
                Intent settingsActivityIntent = new Intent(this, SettingsActivity.class);
                settingsActivityIntent.putExtra("Patient", patient);
                settingsActivityIntent.putExtra("Prescriptions", prescriptions);
                startActivity(settingsActivityIntent);
                finish();
                return true;
            case R.id.menu_edit_dashboard_action_bar_item:
                Intent dashboardActivityIntent = new Intent(this, DashBoardActivity.class);
                startActivity(dashboardActivityIntent);
                finish();
                return true;
            case R.id.menu_edit_prescriptions_action_bar_item:
                // Navigating to the PrescriptionsActivity passing a prescriptions object
                Intent prescriptionsActivityIntent = new Intent(this, PrescriptionsActivity.class);
                prescriptionsActivityIntent.putExtra("Prescriptions", prescriptions);
                startActivity(prescriptionsActivityIntent);
                finish();
                return true;
            case R.id.menu_edit_logout_action_bar_item:
                /*Logging out user, deleting all data in shared preferences file and
                navigating to login activity*/
                sharedPreferences.edit().clear().commit();
                Intent loginActivityIntent = new Intent(this, LoginActivity.class);
                startActivity(loginActivityIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public ArrayList<Appointment> parseAppointmentDatesAndTimes(JSONArray jsonArray){
        ArrayList<Appointment> listOfAppointments = null;
        try{
            listOfAppointments = new ArrayList<Appointment>();
            // Iterating through JSONArray
            for (int i = 0; i < jsonArray.length(); i++){
                // iterating and getting the JSONObjects in the JSONArray
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                // Parsing appointment data into Appointment model
                Appointment appointment = new Appointment();
                appointment.setId(Integer.parseInt(jsonObject.getString("id")));
                appointment.setAppointmentDate(jsonObject.getString("appointmentDate"));
                appointment.setAppointmentTime(jsonObject.getString("appointmentTime"));
                // Add appointment object to list of appointments
                listOfAppointments.add(appointment);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return listOfAppointments;
    }

    public void updateAppointmentButtonClicked(View view){
        // Checking if valid appointment details were provided
        if (isPostDataEntered()){
            Toast.makeText(this, "Editing Appointment...", Toast.LENGTH_SHORT).show();
            // Execute EditAppointmentAsyncTask to edit appointment
            new EditAppointmentAsyncTask().execute(postParams());
        }
        else{
            Toast.makeText(this, "Please select a Date and Time for your appointment!", Toast.LENGTH_SHORT).show();
        }
    }

    private class EditAppointmentAsyncTask extends AsyncTask<ArrayList<String>, Void, String>
    {
        @Override
        protected String doInBackground(ArrayList<String>... params) {
            String response = null;
            try {
                // Executing async task in background thread and returning response
                response = editAppointment(params);
            }catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            try {
                // Checking if response from server is empty or not
                if (response != null){
                    // Parse json data in response
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    // Display success message
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    // Navigate to the DashBoard Activity
                    Intent intent = new Intent(getApplicationContext(), DashBoardActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    String message = "Error: Appointment Not Updated! \n Please Try Again!";
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        private String editAppointment(ArrayList<String>... params){
            String response = null;
            try {
                // Handling HTTP request to server
                // Passing selected appointment id as parameter in url
                URL url = new URL(RestApiUrls.updateAppointment+appointment.getId());
                HttpURLConnection httpURLConnection = HttpConnection.connectToServer(url, "POST");
                // Header data
                String authorization = params[0].get(0);
                // Checking if authorization variable is empty or not
                if (authorization != null){
                    // Assigning header data to Authorization header
                    httpURLConnection.setRequestProperty("Authorization", authorization);
                }
                // data to be sent to the REST-API
                String postParams = "appointmentDate=" + URLEncoder.encode(params[0].get(1), "UTF-8") +
                        "&appointmentTime=" + URLEncoder.encode(params[0].get(2), "UTF-8") +
                        "&gpId=" + URLEncoder.encode(params[0].get(3),"UTF-8");

                httpURLConnection.setFixedLengthStreamingMode(postParams.getBytes().length);
                // Sending request and data to server
                HttpConnection.sendToServer(httpURLConnection, postParams);
                // Checking if HTTP response status code is 200
                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    // Getting response from server
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    // Processing response
                    response = HttpConnection.getResponse(inputStream);
                }
                httpURLConnection.disconnect();
            }catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }
    }

    private class GetAllTimeSlotsAsyncTask extends AsyncTask<ArrayList<String>, Void, String>
    {
        @Override
        protected String doInBackground(ArrayList<String>... params) {
            String response = null;
            try {
                // Executing async task in background thread and returning response
                response = getAllTimeSlots(params);
            }catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            try {
                // Checking if response from server is empty or not
                if (response != null){
                    // Extracting json data from response
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray appointmentsJsonArray = jsonObject.getJSONArray("appointments");
                    // Parsing json data into Appointment model
                    listOfBookedTimeSlots = parseAppointmentDatesAndTimes(appointmentsJsonArray);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        // Method to handle the retrieving of all the booked time slots from server
        private String getAllTimeSlots(ArrayList<String>... params){
            String response = null;
            try {
                // Handling URL and HTTP Requests server
                URL url = new URL(RestApiUrls.getAllTimeSlots);
                HttpURLConnection httpURLConnection = HttpConnection.connectToServer(url, "GET");
                // Checking if HTTP response status code from server is 200
                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    // Getting response from REST-API
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    // Processing response
                    response = HttpConnection.getResponse(inputStream);
                }
                httpURLConnection.disconnect();
            }catch (Exception e){
                e.printStackTrace();
            }
            return response;
        }
    }
}
