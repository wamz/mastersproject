package the.wamz.mastersproject.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import the.wamz.mastersproject.HandleIntents;
import the.wamz.mastersproject.R;
import the.wamz.mastersproject.SharedPreferencesFileName;
import the.wamz.mastersproject.models.Patient;
import the.wamz.mastersproject.models.Prescriptions;

/**
 * Created by The on 25-Jul-15.
 */
public class ViewContactDetailsActivity extends AppCompatActivity
{
    private TextView addressLine1TextView;
    private TextView addressLine2TextView;
    private TextView townCityTextView;
    private TextView countyTextView;
    private TextView postcodeTextView;
    private TextView mobileNumberTextView;
    private TextView telephoneNumberTextView;
    private TextView emailAddressTextView;
    private Patient patient;
    private Prescriptions prescriptions;
    private SharedPreferences sharedPreferences;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_contact_details);

        sharedPreferences = getSharedPreferences(SharedPreferencesFileName.credsFile, MODE_PRIVATE);
        uiElementsSetUp();
        // Getting data passe through an intent
        intent = getIntent();
        patient = HandleIntents.getPatientDetailsFromIntent(intent);
        displayContactDetails(patient);
        prescriptions = HandleIntents.getPrescriptionsFromIntent(intent);
    }

    private void uiElementsSetUp(){
        addressLine1TextView = (TextView) findViewById(R.id.activity_view_contact_details_address_line1_textview);
        addressLine2TextView = (TextView) findViewById(R.id.activity_view_contact_details_address_line2_textview);
        townCityTextView = (TextView) findViewById(R.id.activity_view_contact_details_town_city_textview);
        countyTextView = (TextView) findViewById(R.id.activity_view_contact_details_county_textview);
        postcodeTextView = (TextView) findViewById(R.id.activity_view_contact_details_postcode_textview);
        mobileNumberTextView = (TextView) findViewById(R.id.activity_view_contact_details_mobile_number_textview);
        telephoneNumberTextView = (TextView) findViewById(R.id.activity_view_contact_details_telephone_number_textview);
        emailAddressTextView = (TextView) findViewById(R.id.activity_view_contact_details_email_textview);
    }

    // This method displays patient data in the appropriate textviews
    private void displayContactDetails(Patient patient){
        try {
            // Displaying patient's contact details
            addressLine1TextView.setText(patient.getAddressLine1().toString());
            addressLine2TextView.setText(patient.getAddressLine2().toString());
            townCityTextView.setText(patient.getTownCity().toString());
            countyTextView.setText(patient.getCounty().toString());
            postcodeTextView.setText(patient.getPostcode().toString());
            mobileNumberTextView.setText(patient.getMobileNumber().toString());
            telephoneNumberTextView.setText(patient.getTelephoneNumber().toString());
            emailAddressTextView.setText(patient.getEmail().toString());
            // handling exceptions
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // handling clicks on action bar items
        switch (menuItem.getItemId()){
            case R.id.menu_view_details_settings_action_bar_item:
                // Navigating to the SettingsActivity passing a patient object
                Intent settingsActivityIntent = new Intent(this, SettingsActivity.class);
                settingsActivityIntent.putExtra("Patient", patient);
                settingsActivityIntent.putExtra("Prescriptions", prescriptions);
                startActivity(settingsActivityIntent);
                finish();
                return true;
            case R.id.menu_view_details_edit_details_action_bar_item:
                // Navigating to the EditContactDetailsActivity passing patient and prescriptions object
                Intent editContactDetailsIntent = new Intent(this, EditContactDetailsActivity.class);
                editContactDetailsIntent.putExtra("Patient", patient);
                editContactDetailsIntent.putExtra("Prescriptions", prescriptions);
                startActivity(editContactDetailsIntent);
                return true;
            case R.id.menu_view_details_prescriptions_action_bar_item:
                // Navigating to the PrescriptionsActivity passing a prescriptions object
                Intent prescriptionsActivityIntent = new Intent(this, PrescriptionsActivity.class);
                prescriptionsActivityIntent.putExtra("Prescriptions", prescriptions);
                startActivity(prescriptionsActivityIntent);
                finish();
                return true;
            case R.id.menu_view_details_logout_action_bar_item:
                /* log out, delete data stored in shared preferences file and navigate to
                * login activity */
                sharedPreferences.edit().clear().commit();
                Intent loginActivityIntent = new Intent(this, LoginActivity.class);
                startActivity(loginActivityIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }
}
